package dxt.learn.example;

import dxt.learn.example.demo.hotplugging.MyTriggerManager;
import dxt.learn.example.demo.hotplugging.util.HotPluggingContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);

        MyTriggerManager bean = HotPluggingContext.context.getBean(MyTriggerManager.class);
        System.out.println("2.0");
        System.out.println("1.0");


    }

}
