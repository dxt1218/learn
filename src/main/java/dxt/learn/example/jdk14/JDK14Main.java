package dxt.learn.example.jdk14;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * JDK 14 的语法改动
 */
public class JDK14Main {

    public static void main(String[] args) throws IOException {


        /**
         *  instanceof 类型判断（首次预览）
         */
        // 之前的 instanceof 写法
        Object obj = new ArrayList<>();
        if (obj instanceof ArrayList) {
            ArrayList list = (ArrayList)obj;
            list.add("test");
        }
        System.out.println(obj);
        //在使用 instanceof 判断类型成立后，会自动强制转换类型为指定类型。

        //当前的写法
        Object obj1 = new ArrayList<>();
        if (obj1 instanceof ArrayList list) {
            list.add("test");
        }
        System.out.println(obj1);


        /**
         * 更有用的 NullPointerExceptions提示  TODO 超级赞~~~
         */

        String str = "str";
        String ex = null;
        int length = str.length() + ex.length();
        System.out.println(length);
        //14 之前输出
        //Exception in thread "main" java.lang.NullPointerException
        //at dxt.learn.example.jdk14.JDK14Main.main(JDK14Main.java:36)

        //14 之后输出
        //Exception in thread "main" java.lang.NullPointerException:
        //	Cannot invoke "String.length()" because "ex" is null
        //at dxt.learn.example.jdk14.JDK14Main.main(JDK14Main.java:36)


        /**
         * Switch 表达式 (正式发行)
         * 也是很赞的功能  不用那么累赘的书写switch了
         *
         */

        // 通过传入月份，输出月份所属的季节  jdk 12的第一次预览
        String month12 = "may";
        String season12 = switch (month12) {
            case "march", "april", "may"            -> "春天";
            case "june", "july", "august"           -> "夏天";
            case "september", "october", "november" -> "秋天";
            case "december", "january", "february"  -> "冬天";
            default -> "month erro";
        };

        //jdk 13的第二次预览
        String month13 = "may";
        String season13 = switch (month13) {
            case "march", "april", "may":
                yield "春天";
            case "june", "july", "august":
                yield "夏天";
            case "september", "october", "november":
                yield "秋天";
            case "december", "january", "february":
                yield "冬天";
            default:
                yield "month error";
        };
        //jdk14 让这些 预览功能正式 发行  无需通过命令来开启功能







    }
}


