package dxt.learn.example.jdk13;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * JDK 13 的语法改动
 */
public class JDK13Main {

    public static void main(String[] args) throws IOException {


        /**
         * 文本块 (jdk13 预览功能 )
         * 之前string中放入json 字符串 会出现大量的转义符，从 Java 13 开始你可以使用文本块的方式定义字符串了。
         * 向 Java 语言添加文本块。它避免了大多数转义序列的需要，自动以可预测的方式格式化字符串，并在需要时让开发人员控制格式。
         */
        String oldStr = "{\"name\":\"hh\",\"age\":18}";
        //终于不用写丑陋的长字符串了~~~~~~
        String str13= """
                {"name":"hh","age":18}
                """;


        /**
         *  重新实现 Socket API
         *
         * java.net.Socket 和 java.net.ServerSocket 类早在 Java 1.0 时就已经引入了，
         * 它们的实现的 Java 代码和 C 语言代码的混合，维护和调试都十分不易；而且这个实现还存在并发问题，有时候排查起来也很困难。
         *
         * 底层 采用 NioSocketImpl 新的实现类 替代PlainSocketImpl
         *
         * 作者支持回退 -Djdk.net.usePlainSocketImpl 命令可以切回旧的实现。 还是很有考虑到的。
         */

        ServerSocket serverSocket = new ServerSocket(8000);
        Socket clientSocket = serverSocket.accept();
        // jdk 13 之后  底层类记载的是  sun.net.PlatformSocketImpl
        // jdk 13 之前  底层类记载的是  java.net.PlainSocketImpl


    }
}


