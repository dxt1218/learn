package dxt.learn.example.jdk9.privateImter;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

/**
 * Java 8 允许在接口中编写默认方法，这是一个广受欢迎的功能。从 Java 9 开始，你可以在接口中包含私有方法。
 * 私有接口方法
 *
 * 规则：
 * 私有接口方法不能是抽象的。
 * 私有方法只能在接口内部使用。
 * 私有静态方法可以在其他静态和非静态接口方法中使用。
 * 私有非静态方法不能在私有静态方法中使用
 * @author DuanXT
 * @date 2022/12/12 16:07
 */
public interface Custom {

    default int addEvenNumbers(int... nums) {
        return add(n -> n % 2 == 0, nums);
    }

    default int addOddNumbers(int... nums) {
        return add(n -> n % 2 != 0, nums);
    }

    //私有的底层方法
    private int add(IntPredicate predicate, int... nums) {
        return IntStream.of(nums)
                .filter(predicate)
                .sum();
    }

}
