package dxt.learn.example.jdk9.streamApi;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stream（流） API改进
 * Java 9 引入了两种与 Streams 交互的新方法，即takeWhile/dropWhile方法。
 *
 */
public class NewStreamApi {

    /**
     * 新方法takeWhile。dropWhile允许开发者基于谓词获取流的一部分
     */
    public static void main(String[] args) {

        List<String> alphabets = List.of("a", "b", "c", "d", "e", "f", "g", "h", "i");
        List<String> one = alphabets
                .stream()
                .takeWhile(s -> !s.equals("d"))  //takeWhile: 从头开始筛选，遇到不满足的就结束了。
                .collect(Collectors.toList());
        //打印出：[a, b, c]
        System.out.println(one);

        List<String> alphabets2 = List.of("a", "b", "c", "d", "e", "f", "g", "h", "i");
        List<String> subset2 = alphabets2
                .stream()
                .dropWhile(s -> !s.equals("d")) //odropWhile: 从头开始删除，遇到不满足的就结束了。
                .collect(Collectors.toList());
        //打印出：[d, e, f, g, h, i]
        System.out.println(subset2);


        /**
         * TODO 爆赞~~~~ 好用
         * 此外，它还添加了两个重载方法，即ofNullable和iterate方法，类似option的用法。
         */
        //在 Java 8 之前，流中不能有null值。它会导致NullPointerException.
        // 从 Java 9 开始，Stream.ofNullable()方法允许您创建一个单元素流，该流包装一个不为null的值，否则为空流。.
        Stream.ofNullable(alphabets2).collect(Collectors.toList());

        /**
         * 在 Stream 增强之外，还增强了 Optional ，Optional 增加了可以转换成 Stream 的方法。
         */
        Stream<Integer> s = Optional.of(1).stream();
        s.forEach(System.out::print);
    }
}
