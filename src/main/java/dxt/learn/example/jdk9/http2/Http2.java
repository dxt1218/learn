package dxt.learn.example.jdk9.http2;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 *
 * Java 9 引入了新的 API，它使用起来更干净、更清晰，并且还增加了对 HTTP/2 的支持。
 * 新 API 使用 3 个主要类HttpClient，HttpRequest和HttpResponse
 * @author DuanXT
 * @date 2022/12/12 16:10
 */
public class Http2 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {


       /* HttpClient client = HttpClient.newHttpClient();
        URI uri = URI.create("http://www.baidu.com");
        HttpRequest req = HttpRequest.newBuilder(uri).header("User-Agent", "Java").GET().build();
        HttpResponse<String> resp = client.send(req, HttpResponse.BodyHandler.asString());
        String body = resp.body();
        System.out.println(body);
        System.out.println(httpResponse.body());
*/
        /**
         * httpClient.sendAsync()API 还支持使用方法的异步 HTTP 请求。
         * 它返回CompletableFuture可用于确定请求是否已完成。
         * 它还可以在HttpResponse请求完成后访问。如果你愿意，甚至可以在请求完成之前取消它。
         */

        /*if(httpResponse.isDone()) {
            System.out.println(httpResponse.get().statusCode());
            System.out.println(httpResponse.get().body());
        } else {
            httpResponse.cancel(true);
        }*/
    }

}
