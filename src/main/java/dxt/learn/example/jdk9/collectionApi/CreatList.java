package dxt.learn.example.jdk9.collectionApi;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * 从 Java 9 开始，您可以使用新的工厂方法创建不可变集合，
 * 例如不可变list、不可变set和不可变map。
 *
 */
public class CreatList {

    public static void main(String[] args) {
        /**
         * 在 Java 9 中为集合的创建增加了静态工厂创建方式，也就是 of 方法，通过静态工厂 of 方法创建的集合是只读集合，里面的对象不可改变。
         * 并在不能存在 null 值，对于 set 和 map 集合，也不能存在 key 值重复。
         *
         * 优点：这样不仅线程安全，而且消耗的内存也更小。
         * 消耗更小的体现 ：
         * 静态工厂 of 方法创建的集合还有一个特性，就是工厂内部会自由复用已有实例或者创建新的实例，
         * 所以应该避免对 of 创建的集合进行判等或者 haseCode 比较等操作。
         */
        List<String> namesList = List.of("Lokesh", "Amit", "John");

        Set<String> namesSet = Set.of("Lokesh", "Amit", "John");

        Map<String, String> namesMap = Map.ofEntries(
                Map.entry("1", "Lokesh"),
                Map.entry("2", "Amit"),
                Map.entry("3", "Brian"));

        Map<String, Integer> stringIntegerMap = Map.of("key1", 1, "key2", 2, "key3", 3);

        /**
         * 这种只读集合在 Java 9 之前创建是通过 Collections.unmodifiableList 修改集合操作权限实现的。
         *
         * 设置为只读集合
         *  namesList = Collections.unmodifiableList(namesList);
         */
        namesList = Collections.unmodifiableList(namesList);



    }

}
