package dxt.learn.example.jdk9;

public class Jdk9Main {

    /**
     * 从 Java 9 开始，@Deprecated注解将具有两个属性，即forRemoval和since.
     * @deprecated  forRemoval – 指示带注释的元素是否会在未来版本中被删除。
     *             since - 它返回注释元素被弃用的版本。
     */
    @Deprecated(forRemoval = true,since = "jdk9")
    public static void main(String[] args) {
        /**
         * 比较有意思的功能 根据jdk版本 来决定加载的class
         *
         * META-INF 包含一个版本子目录
         * 使用多版本特性，现在一个 jar 可以包含一个类的不同版本——兼容不同的 JDK 版本。
         * 官方案例：
         * jar root
         *   - A.class
         *   - B.class
         *   - C.class
         *   - D.class
         *   - META-INF
         *      - versions
         *         - 9
         *            - A.class
         *            - B.class
         *         - 10
         *            - A.class
         */
    }
}
