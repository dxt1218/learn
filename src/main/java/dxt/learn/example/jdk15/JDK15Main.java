package dxt.learn.example.jdk15;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * JDK 15 的语法改动
 */
public class JDK15Main {
    public static void main(String[] args) throws IOException {

        //instanceof 类型匹配 (二次预览功能 )


        /**
         * 文本块 (正式发布)
         */
        String oldStr = "{\"name\":\"hh\",\"age\":18}";
        //终于不用写丑陋的长字符串了
        String str15= """
                {"name":"hh","age":18}
                """;

        record  User(){}

        /**
         * TreeMap  新增的方法
         */
        TreeMap<String, User> stringMap = new TreeMap<>();


    }
}


