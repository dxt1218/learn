package dxt.learn.example.jdk8.defaulable;

import java.util.function.Supplier;

/**
 */
public interface DefaulableFactory {

    // 接口现在允许静态方法
    static Defaulable create( Supplier<Defaulable> supplier ) {
        return supplier.get();
    }
    interface DefaulableFactory2 {
        static Defaulable create( Supplier<Defaulable> supplier ) {
            return supplier.get();
        }
    }

    public static void main( String[] args ) {
        Defaulable defaulable = DefaulableFactory.create( DefaultMain.DefaultableImpl::new );
        System.out.println( defaulable.notRequired() );

        defaulable = DefaulableFactory.create( DefaultMain.OverridableImpl::new );
        System.out.println( defaulable.notRequired() );
    }
}
