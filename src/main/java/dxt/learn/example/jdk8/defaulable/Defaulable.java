package dxt.learn.example.jdk8.defaulable;

/**
 * @author DuanXT
 * @date 2022/10/26 11:41
 */
public  interface Defaulable {
    // Interfaces now allow default methods, the implementer may or
    // may not implement (override) them.
    default String notRequired() {
        return "Default implementation";
    }
}
