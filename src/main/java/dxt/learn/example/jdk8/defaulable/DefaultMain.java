package dxt.learn.example.jdk8.defaulable;

/**
 *
 *
 */
public class DefaultMain {

    public static class DefaultableImpl implements Defaulable {
    }

    public  static class OverridableImpl implements Defaulable {
        @Override
        public String notRequired() {
            return "Overridden implementation";
        }
    }

    public static void main( String[] args ) {
        Defaulable defaulable = DefaulableFactory.create( DefaultableImpl::new );
        System.out.println( defaulable.notRequired() );

        defaulable = DefaulableFactory.create( OverridableImpl::new );
        System.out.println( defaulable.notRequired() );

    }
}
