package dxt.learn.example.jdk8.optionals;

import java.util.Optional;

/**
 * Optional
 */
public class OptionalMain {
    public static void main(String[] args) {
        Object obj = new Object();
        Optional<Object> o = Optional.of(obj);
    }
}
