package dxt.learn.example.jdk11;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

/**
 * JDK11的语法变更
 * @author DuanXT
 * @date 2022/12/23 10:40
 */
public class JDK11LMain {


    public static void main(String[] args) throws IOException, InterruptedException {

        ArrayList<Object> list = new ArrayList<>();
        list.toArray();


        /**
         * =================String 新增api功能========================================
         */
        String str= "aa\nbb";

        //判空，blank
        System.out.println(str.isBlank());

        //该方法是根据 换行符 \n  或者回车  \r 或者回车换行  \r\n  进行分割
        Stream<String> lines = str.lines();
        lines.forEach(System.out::println);

        //复制字符串
        String str1= "abc";
        String repeat = str1.repeat(2);
        System.out.println(repeat);

        /**
         * 输出
         * false
         * aa
         * bb
         * abcabc
         */

        // 去除前后空白
        String strip = "   　 string字符 　";
        System.out.println("==" + strip.trim() + "==");
        // 去除前后空白字符，如全角空格，TAB
        System.out.println("==" + strip.strip() + "==");
        // 去前面空白字符，如全角空格，TAB
        System.out.println("==" + strip.stripLeading() + "==");
        // 去后面空白字符，如全角空格，TAB
        System.out.println("==" + strip.stripTrailing() + "==");

        // 输出
        // ==　 string字符 　==
        // ==string字符==
        // ==string字符 　==
        // ==   　 string字符==
        /**这里注意，trim 只能去除半角空格，而 strip 是去除各种空白符。*/



        /**=================
         * File API改动
         * 读写文件变得更加方便。
         * ========================================*/
        // 创建临时文件
        Path path = Files.writeString(Files.createTempFile("test", ".txt"),  "https://www.baidu.com");
        System.out.println(path);
        // 读取文件
        // String ss = Files.readString(Path.of("file.json"));
        String s = Files.readString(path);
        System.out.println(s);



        /**=================
         * HTTP Client
         * 在 Java 11 中 Http Client API 得到了标准化的支持。且支持 HTTP/1.1 和 HTTP/2 ，也支持 websockets。
         * http://openjdk.java.net/groups/net/httpclient/recipes-incubating.html
         * HTTPClient 已经在 Java11中标准化了。
         * ========================================*/

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://www.weibo.com"))
                .build();
        // 异步
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();

        // 同步
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());


        /**=================
         *
         * Lambda 局部变量推断
         * 集合jdk10引入的var语法，在jdk11的时候，这个语法糖可以在Lambda中进行使用。
         *
         * ========================================*/
        var hashMap = new HashMap<String, Object>();
        hashMap.put("A", "a");
        hashMap.put("B", "b");
        hashMap.forEach((var k, var v) -> {
            System.out.println(k + ": " + v);
        });
        //这里需要注意的是，(var k,var v) 中，k 和 v 的类型要么都用 var ，要么都不写，要么都写正确的变量类型。而不能 var 和其他变量类型混用。


        /**
         *  单命令运行 Java
         *  之前运行一个java 程序
         *  1. javac 编译字节码
         *  2. java 运行程序class文件
         *
         * jdk11之后
         * $ java  xxx.java   即可运行
         */


    }
}
