package dxt.learn.example.jdk10;

import java.util.*;

/**
 * @author DuanXT
 * @date 2022/12/19 18:33
 */
public class TestJDK10 {

    public static void main(String[] args) {

        var list = new ArrayList<String>();
        list.add("a");
        list.add("b");


        List<String> listCopy = List.copyOf(list);
        //Arrays.asList()
        Optional.of(list).orElseThrow();

        /**
         * 不可变工具集合是为何不可变嘞？
         * ListN  实体  ImmutableCollections.ListN  内部类
         * 类似Arrays.asList()方法  返回的不是我们认为list对象 而是一个内部类。这个内部类 没有实现List的相关功能方法。
         */



        /**
         * 字节码生成已经得到改进，增强了 For 循环。
         */
        List<String> data = new ArrayList<>();
        for (String b : data) {
        }
        ;

        /**
         * 编译后:
         * Iterator i$ = data.iterator();
         * for (; i$.hasNext(); ) { String b = (String)i$.next(); } b = null; i$ = null; }
         * 这行编译后的代码有什么特点 ？
         * *//*
        Iterator i$ = data.iterator();
        for (; i$.hasNext(); ) {
            String b = (String) i$.next();
        }
        b = null;
        i$ = null;*/



        /**
         * 这么编译的有点 ：
         *  在 for 循环之外声明迭代器变量可以在不再使用它时立即为其赋值 null。
         *          * 这使得 GC 可以访问它，然后 GC 可以处理掉未使用的内存。当增强的 for 循环中的表达式是一个数组时，也会执行类似的操作。
         *
         *  衍生问题 为何要在for循环后做该编译优化操作？
         *
         */







        /**
         *  衍生问题 为何要在for循环后做该编译优化操作？
         *
         * 个人理解  是为了配合GC ，GC中的 Safe Point 安全点的设置
         * 安全点位置的选取基本上是以“是否具有让程序长时间执行的特征”为标准 进行选定的，
         * 因为每条指令执行的时间都非常短暂，程序不太可能因为指令流长度太长这样的原因而 长时间执行，“长时间执行”的最明显特征就是指令序列的复用，
         * 例如方法调用、循环跳转、异常跳转 等都属于指令序列复用，所以只有具有这些功能的指令才会产生安全点。
         *
         * 只有到达某些点才可以进行GC操作，这些点称作安全点（Safepoint），比如，循环的末尾、方法临返回前/调用方法的call指令后、可能跑异常的位置等。
         */

        /**
         * 安全点：
         *
         * 在OopMap的协助下，HotSpot可以快速准确地完成GC Roots枚举，但随之而来的一个现实问题：可能导致引用关系变化，或者说导致OopMap内容变化的指令非常之多，如果为每一条指令都生成对应的OopMap，那将会需要大量的额外存储空间，这样垃圾收集伴随而来的空间成本就会非常高昂。
         *
         * 在实际上，也的确没有为每条指令都生成OopMap，只是在“特定的位置”记录 了这些信息，这些位置被称为安全点（Safepoint）。
         * 有了安全点的设定，也就决定了用户程序执行时 并非在代码指令流的任意位置都能够停顿下来开始垃圾收集，而是强制要求必须执行到达安全点后才能够暂停。
         */





    }
}
