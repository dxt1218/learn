package dxt.learn.example.jdk10;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * JEP 286    var  局部类型推断
 *
 * 让 Java 可以像 Js 里的 var 或者其他语言的 auto 一样可以自动推断数据类型。
 * 这其实只是一个新的语法糖，底层并没有变化，在编译时就已经把 var 转化成具体的数据类型了，但是这样可以减少代码的编写。
 *
 */
public class VarTest {

public static void main(String[] args) {
    var hashMap = new HashMap<String, String>();
    hashMap.put("a","b");
    var string = "hello java 10";
    var stream = Stream.of(1, 2, 3, 4);
    var list = new ArrayList<String>();
    //编译后一样的
    /**
     *         HashMap<String, String> hashMap = new HashMap();
     *         hashMap.put("a", "b");
     *         String string = "hello java 10";
     *         Stream<Integer> stream = Stream.of(1, 2, 3, 4);
     *         new ArrayList();
     *
     *         缺点也很明显：
     *         一眼并不能看出 result 的数据类型。
     */
}

    public static void testVar() {
        // 情况1，没有初始化会报错
        // var list;
        var list = List.of(1, 2, 3, 4);

        // 情况2
        for (var integer : list) {
            System.out.println(integer);
        }

        // 情况3
        for (var i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

}
