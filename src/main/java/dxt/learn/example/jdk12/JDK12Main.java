package dxt.learn.example.jdk12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * JDK 12 的语法改动
 */
public class JDK12Main {

    public static void main(String[] args) throws IOException {
        /**
         * 文件对比 Files.mismatch
         * 对比两个文件内容是否一致，如果内容一致，会返回 -1  ，如果内容不同，会返回不同的字节开始位置。
         */
        // 创建两个临时文件
        Path aFile = Files.createFile(Paths.get("A.txt"));
        Path bFile = Files.createFile(Paths.get("B.txt"));

        // 写入相同内容
        Files.write(aFile,"123456".getBytes(), StandardOpenOption.WRITE);
        Files.write(bFile,"123456".getBytes(), StandardOpenOption.WRITE);
        long mismatch = Files.mismatch(aFile, bFile);
        System.out.println(mismatch);

        // 追加不同内容
        Files.write(aFile,"789".getBytes(), StandardOpenOption.APPEND);
        Files.write(bFile,"987".getBytes(), StandardOpenOption.APPEND);
        mismatch = Files.mismatch(aFile, bFile);
        System.out.println(mismatch);

        // 删除创建的文件
        aFile.toFile().deleteOnExit();
        bFile.toFile().deleteOnExit();
        //输出
        //-1  相同
        //6   6下标  从0开始的话的 也就是第七位开始不同 正好是上面追加的点


        /**
         *  紧凑数字格式化的支持 NumberFormat 添加了对紧凑形式的数字格式化的支持。紧凑型数字格式是指以简短或人类可读的形式表示数字。
         *  例如，在 en _ US 区域设置中，根据 NumberFormat 指定的样式，
         *  1000可以格式化为“1K”，1000000可以格式化为“1M”。风格。
         *  紧凑数字格式由 LDML 的紧凑数字格式规范定义
         */
        System.out.println("Compact Formatting is:");
        NumberFormat upvotes = NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.SHORT);

        System.out.println(upvotes.format(100));
        System.out.println(upvotes.format(1000));
        System.out.println(upvotes.format(10000));
        System.out.println(upvotes.format(100000));
        System.out.println(upvotes.format(1000000));

        // 设置小数位数
        upvotes.setMaximumFractionDigits(1);
        System.out.println(upvotes.format(1234));
        System.out.println(upvotes.format(123456));
        System.out.println(upvotes.format(12345678));
        //100
        //1K
        //10K
        //100K
        //1M
        //1.2K
        //123.5K
        //12.3M


    }
}


