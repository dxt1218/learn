package dxt.learn.example.demo.li;

import dxt.learn.example.demo.hotplugging.annotation.HotPluggingMethod;
import org.springframework.stereotype.Component;

/**
 * @author DuanXT
 * @date 2022/12/26 16:53
 */
public class Ex2 {

    @HotPluggingMethod
    public static String say(String str){

        return "";
    }
}
