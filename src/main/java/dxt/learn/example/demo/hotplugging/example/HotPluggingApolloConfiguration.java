package dxt.learn.example.demo.hotplugging.example;


import dxt.learn.example.demo.hotplugging.config.HostPluggingProcessor;
import dxt.learn.example.demo.hotplugging.config.HotPlugging;
import dxt.learn.example.demo.hotplugging.config.HotPluggingConfiguration;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 两种例子模板 注入
 * 配置例子 apollo形式  springboot @Bean example
 */
@Configuration
@Import({HostPluggingProcessor.class})
public class HotPluggingApolloConfiguration  {

    @Bean
    @ConditionalOnMissingBean(HotPlugging.class)
    @ConditionalOnProperty(prefix = "apollo", value = "meta")   //自己定义相关的配置条件
    HotPlugging  TradeApolloConfiguration(){
        System.out.println("交易 apollo配置 初始化");
        return new HotPlugging();
    }


    @Bean
    @ConditionalOnMissingBean(HotPlugging.class)
    @ConditionalOnProperty(prefix = "gfm.apollo.extra", value = "enable", havingValue = "true")  //自己定义相关的配置条件
    HotPlugging  otherApolloConfiguration(){
        System.out.println("用户/商品 apollo配置 初始化");
        return new HotPlugging();
    }




}
