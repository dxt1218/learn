package dxt.learn.example.demo.hotplugging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;


/**
 * My触发管理器
 * 当admin项目 异常 或者 宕机后 save oneself
 * @author DuanXT
 */
@Configuration
@Conditional(MyConfigEnabledCondition.class)
public class MyTriggerManager {

    private static Logger logger = LoggerFactory.getLogger(MyTriggerManager.class);

    public MyTriggerManager (){
    }

}
