package dxt.learn.example.demo.hotplugging.annotation;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Indexed;

import java.lang.annotation.*;

/**
 * 声明这个是 热插拔控制类
 * 如果实在spring 中被@Component 修饰的类无需声明
 * 如果是静态  工具类  则需要声明。
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Indexed
public @interface HotPluggingSupport {

    /**
     * 声明方法
     * @return
     */
    String[] methods() default {};

}
