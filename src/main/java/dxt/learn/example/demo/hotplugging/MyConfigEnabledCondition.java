package dxt.learn.example.demo.hotplugging;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class MyConfigEnabledCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String enabled = context.getEnvironment().getProperty("job.save.oneself.enabled");
        return Boolean.parseBoolean(enabled);
    }
    public static void main(String[] args) {
        int number = -8;
        int bitCount = Integer.SIZE; // 位数，默认为 int 类型的位数

        // 将数字转换为二进制字符串
        String binaryString = Integer.toBinaryString(number);
        System.out.println(binaryString);
        int number1 = number>>1;
        System.out.println(Integer.toBinaryString(number1));
        System.out.println(number1);

        int number2 = number>>>1;
        String binaryString2 = Integer.toBinaryString(number2);

        System.out.println(binaryString2);

        System.out.println(number2);
    }
}


