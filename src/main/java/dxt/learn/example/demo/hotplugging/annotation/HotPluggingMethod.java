package dxt.learn.example.demo.hotplugging.annotation;


import java.lang.annotation.*;

/**
 * 声明这个是 热插拔方法
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HotPluggingMethod {

    /**
     * 控制key  作用于apollo等 config server
     * @return
     */
    String switchKey() default "";

}
