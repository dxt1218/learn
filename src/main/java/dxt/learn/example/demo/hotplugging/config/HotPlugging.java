package dxt.learn.example.demo.hotplugging.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportSelector;

/**
 * 基础配置
 * 推荐  hot.plugging.enable = true 的形式 开启
 */
@Configuration
@ConditionalOnProperty(prefix = "hot.plugging", value = "enable", havingValue = "true")  //自己定义相关的配置条件
public class HotPlugging  {

}
