package dxt.learn.example.demo.hotplugging.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


/**
 * 当前项目spring的上下文
 */
@Component
public class HotPluggingContext implements ApplicationContextAware {

    /**
     * 直接使用 context.getBean  或者用  doHotPlugging 方法获得 bean
     */
    public  static ApplicationContext context;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (null == context){
            this.context = applicationContext;
        }
    }


    /**
     * 通过spring容器里的热插拔对象
     * @param clazz
     * @return
     * @param <T>
     */
    public static<T> T getHotPluggingBean(Class<T> clazz){
        return context.getBean(clazz);
    }



}
