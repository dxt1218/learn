package dxt.learn.example.demo.hotplugging.config;

import dxt.learn.example.demo.hotplugging.util.HotPluggingContext;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;






@Configuration
//@ConditionalOnMissingBean(HotPlugging.class)
public class HotPluggingConfiguration  implements  ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {
        registerBeanDefinitions(importingClassMetadata, registry);
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        registry.registerBeanDefinition("hotPluggingContext",
                BeanDefinitionBuilder.genericBeanDefinition(HotPluggingContext.class).getBeanDefinition());

        registry.registerBeanDefinition("hostPluggingProcessor",
                BeanDefinitionBuilder.genericBeanDefinition(HostPluggingProcessor.class).getBeanDefinition());
    }


}
