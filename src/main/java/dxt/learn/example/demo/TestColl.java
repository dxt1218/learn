package dxt.learn.example.demo;

import com.alibaba.fastjson2.JSON;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@RestController
public class TestColl {

    @RequestMapping("/aa/test")
    public void Test(HttpServletRequest request, HttpServletResponse response){
        Enumeration<String> headerNames = request.getHeaderNames();
        System.out.println(JSON.toJSONString(headerNames));
        System.out.println();
        System.out.println();
        System.out.println();

    }

}
