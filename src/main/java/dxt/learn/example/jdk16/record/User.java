package dxt.learn.example.jdk16.record;

import java.util.ArrayList;
import java.util.List;

/**
 * JDK.17
 * 记录类
 */
public class User {

    public String name;

    public String age;


    //

    record Student(String name,int age) {

    }



    public List<String> handleList(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.stream().filter("eq"::equals).forEach(System.out::println);
        return arrayList;
    }

}
