package dxt.learn.example.jdk16;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * JDK 16 的语法改动
 */
public class JDK16Main {
    public static void main(String[] args) throws IOException {

        Object obj = new String();
        /**
         *  instanceof 类型匹配  正式发行 todo 也是好用的
         */
        if (obj instanceof String) {
            String s = (String) obj;
        }
        //当前的写法
        if (obj instanceof String s) {
            s.length();
            // Let pattern matching do the work!
            //.....
        }

        //public final boolean equals(Object o) {
        //    return (o instanceof Point other)
        //        && x == other.x
        //        && y == other.y;
        //}


        /**
         *         ==============
         *         Add Stream.toList() Method  TODO 好用~~
         *`stream.collect(Collectors.toList())` 和 `stream.collect(Collectors.toSet())`
         * 最受欢迎的方法引来的简化和封装 。
         *
         *         ====================
         */
        record  User(String name){}
        List<User> users = new ArrayList<>();
        List<String> strings = users.stream().map(User::name).toList();
        // 等价 users.stream().map(User::name).collect(Collectors.toList());


    }
}


