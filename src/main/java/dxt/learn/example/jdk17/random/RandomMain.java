package dxt.learn.example.jdk17.random;

import java.util.concurrent.ThreadLocalRandom;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

/**
 *增强的伪随机数生成器 Enhanced Pseudo-Random Number Generators
 */
public class RandomMain {

/*
    public static void main(String[] args) {
        ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        System.out.println(randomGenerator.nextInt(10));
    }
*/
    //基础用法
    public static void main(String[] args) {

        //基础用法
        RandomGeneratorFactory<RandomGenerator> l128X256MixRandom = RandomGeneratorFactory.of("L128X256MixRandom");
        // 使用时间戳作为随机数种子
        RandomGenerator randomGenerator = l128X256MixRandom.create(System.currentTimeMillis());
        for (int i = 0; i < 5; i++) {
            System.out.println(randomGenerator.nextInt(10));
        }
        //你也可以遍历出所有的 PRNG 算法。
        RandomGeneratorFactory.all().forEach(factory -> {
            System.out.println(factory.group() + ":" + factory.name());
        });

        //System.out.println:
        //LXM:L32X64MixRandom
        //LXM:L128X128MixRandom
        //LXM:L64X128MixRandom
        //Legacy:SecureRandom
        //LXM:L128X1024MixRandom
        //LXM:L64X128StarStarRandom
        //Xoshiro:Xoshiro256PlusPlus
        //LXM:L64X256MixRandom
        //Legacy:Random
        //Xoroshiro:Xoroshiro128PlusPlus
        //LXM:L128X256MixRandom
        //Legacy:SplittableRandom
        //LXM:L64X1024MixRandom

        /**
         * 可以看到 Legacy:Random 也在其中，新的 API 兼容了老的 Random 方式，
         * 所以你也可以使用新的 API 调用 Random 类生成随机数。
         */
        // 使用 Random
        RandomGeneratorFactory<RandomGenerator> Random = RandomGeneratorFactory.of("Random");
        // 使用时间戳作为随机数种子
        RandomGenerator random = Random.create(System.currentTimeMillis());
        for (int i = 0; i < 5; i++) {
            System.out.println(random.nextInt(10));
        }


    }


}
