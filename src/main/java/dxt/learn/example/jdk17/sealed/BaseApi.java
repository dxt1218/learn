package dxt.learn.example.jdk17.sealed;

/**
 * @author DuanXT
 * @date 2022/10/13 17:35
 */
public sealed interface BaseApi permits BaseObj.SunBo, BaseObj.SunObj {
    void sayHi();
}
