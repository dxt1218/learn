package dxt.learn.example.jdk17.sealed;

/**
 * @author DuanXT
 * @date 2022/10/18 12:20
 */
public class SealedMain {

    public static void test(BaseSealedService sealedService) {
        /**
         *
         *
         *Shape rotate(Shape shape, double angle) {
         *         if (shape instanceof Circle) return shape;
         *         else if (shape instanceof Rectangle) return shape;
         *         else if (shape instanceof Square) return shape;
         *         else throw new IncompatibleClassChangeError();
         * }
         *
         * Shape rotate(Shape shape, double angle) {
         *     return switch (shape) {   // pattern matching switch
         *         case Circle c    -> c;
         *         case Rectangle r -> shape.rotate(angle);
         *         case Square s    -> shape.rotate(angle);
         *         // no default needed!
         *     }
         * }
         */
    }




}
