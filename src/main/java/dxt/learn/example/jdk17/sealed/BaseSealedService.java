package dxt.learn.example.jdk17.sealed;

/**
 * @author DuanXT
 * @date 2022/10/18 12:19
 */
public sealed  interface BaseSealedService permits BaseSealedServiceImp, BaseSealedServiceImp2 {
}
