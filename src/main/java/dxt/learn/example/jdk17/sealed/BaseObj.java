package dxt.learn.example.jdk17.sealed;

/**
 * JDK17
 * 密封类 ：密封的类或接口只能由允许这样做的类和接口扩展或实现。
 * sealed 修饰符  密封类必须有子类
 * permits 指定许可
 *
 * 作者设计动机：
 *      限制一个接口或者一个类的扩展。
 *      超类应该可以被广泛访问，但不能被广泛扩展。
 *
 * JDK中的应用示例：AbstractStringBuilder  是以包私有的形式，两个子类：StringBuffer、StringBuilder。
 *
 * 密封类可类似枚举使用在Switch中作为case条件进行判断，无需进项 instance of 的语句判断并转换。
 */
public sealed class BaseObj permits BaseObj.SunObj {

    final class SunObj extends BaseObj implements BaseApi {

        @Override
        public void sayHi() {

        }
    }

    record  SunBo() implements BaseApi{

        @Override
        public void sayHi() {

        }
    }
    //应用在java.long.constant包中ConstantDesc
}
