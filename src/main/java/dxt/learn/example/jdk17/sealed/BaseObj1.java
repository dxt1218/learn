package dxt.learn.example.jdk17.sealed;

/**
 * JDK17
 * 密封类
 * sealed 修饰符  密封类必须有子类
 * permits 指定许可
 */
public sealed class BaseObj1  {
    //这种声明写法  可以省略掉  permits关键字
    //编译器可以自己识别推断出允许的子类
    //编译器可以判断出 baseObj1对象 允许三个sunObj的子类
    final class SunObj1 extends BaseObj1 {

    }

    final class SunObj2 extends BaseObj1 {

    }

    final class SunObj3 extends BaseObj1 {

    }
}
