package dxt.learn.example.jdk17.strict;


public class StrictfpMain {

    /**
     *恢复始终严格的浮点语义
     */
    public static void main(String[] args) {
        testStrictfp();
    }

    public  strictfp   static void testStrictfp() {
        System.out.println(255555555f - 255555554f);
        System.out.println(25555555f - 25555554f);
        System.out.println(1.7 - 1.6);
        double a = 0.05;
        double b = 0.01;
        double sum =a+b;
        System.out.println(sum);
        //这样的结果如果是在银行或者其他商业计算中出现那么结果是不能想像的。所以有严格的浮点计算。JAVA对这样的计算是封装在BigDecimal类中的。
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
        //官方的例子：
        double d = 8e+307;
        System.out.println(4.0 * d * 0.5);
        System.out.println(2.0 * d);
        //Double.MAX_VALUE大概是1.8e+308
        /**
         *
         * 扩展指数范围
         * 体会一下，也就是在某种情况下，数字的范围不一样。并不限定于32位float和64位double了。上面的计算结果，可能就不是Infinity，而是1.6E308或其他值。
         * 一旦使用了strictfp来声明一个类、接口或者方法时，那么所声明的范围内Java的编译器以及运行环境会完全依照浮点规范IEEE-754来执行。
         *
         * IEEE-754到底是个什么东东呢？
         * IEEE二进制浮点数算术标准（IEEE 754）是最广泛使用的浮点数运算标准，为许多CPU与浮点运算器所采用。  http://zh.wikipedia.org/wiki/IEEE_754\
         * 大致意思是：在一个FP-strict表达式中间的所有值必须是float或者double，这意味着运算结果也肯定是IEEE754中所规定的单精度和双精度数，也即是32位的float和64位的double。
         * 这里也只说明了，float和double的规定，是符合IEEE754标准的。
         *
         * 1.strictfp翻译为“精确的浮点”不够贴切，容易引起误解。
         *
         * 2.strictfp关键字的使用与IEEE754没有直接因果关系。IEEE 754，是IEEE制定的，而不是J2EE标准:)
         *
         * 3.使用strictfp关键字的目的，是保证平台移植之后，浮点运算结果是一致的。
         */
    }

}
