package dxt.learn.example.jdk17.record;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.RecordComponent;

/**
 * JDK.16  JEP 395   https://openjdk.org/jeps/395
 *
 * 特点：
 *      Record是 Java 语言中的一种新类。
 *      它们充当不可变数据的透明载体，不像普通类那样拘谨。
 *      记录类有助于以比普通类更少的形式对普通数据聚合进行建模。
 * 动机：
 *    常见的抱怨是“Java 太冗长”或“太多仪式”。
 *     正确编写这样的数据载体类涉及大量低价值、重复、容易出错的代码：构造函数、访问器、equals、hashCode、toString等。
 *
 * 语法写法：
 *          写法：
 *          public record Teacher(String name, String className) {
 *
 *          }
 *
 *          编译后：
 *
 *          public record Teacher(String name, String className) {
 *           //隐式属性被final修饰
 *          private final String name;
 *          private final String className;
 *
 *
 *          Point(String name, String className) {
 *                  this.name= name;
 *                  this.className = className;
 *             }
 * }
 *
 */
@Slf4j
public record Teacher(String name, String className) {
    public String sayHi(){
        log.info("a");
        return "a";
    }

    /**
     * 本地记录类嵌套记录类的一个特例
     * 在以下示例中，商家和月销售数据的聚合使用本地记录类 建模MerchantSales。使用这个记录类提高了以下流操作的可读性：
     *
     * List<Merchant> findTopMerchants(List<Merchant> merchants, int month) {
     *
     *     // Local record 本地记录类
     *     record MerchantSales(Merchant merchant, double sales) {}
     *
     *     return merchants.stream()
     *         .map(merchant -> new MerchantSales(merchant, computeSales(merchant, month)))
     *         .sorted((m1, m2) -> Double.compare(m2.sales(), m1.sales()))
     *         .map(MerchantSales::merchant)
     *         .collect(toList());
     * }
     */

  public static void main(String[] args) {
      Teacher teacher = new Teacher("b","c");
      Class<? extends Teacher> aClass = teacher.getClass();
      System.out.println(aClass);
      /**
       * 两个新增的反射方法
       */
      RecordComponent[] recordComponents = aClass.getRecordComponents();
      System.out.println(aClass.isRecord());
      System.out.println(User.class.getSuperclass());


  }


}

