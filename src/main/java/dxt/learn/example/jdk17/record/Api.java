package dxt.learn.example.jdk17.record;

/**
 * @author DuanXT
 * @date 2022/10/12 17:15
 */
public interface Api {
    String sayHi();
}
