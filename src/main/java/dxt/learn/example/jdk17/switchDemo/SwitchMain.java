package dxt.learn.example.jdk17.switchDemo;

/**
 * jdk17提出的预览功能
 * switch的模式匹配
 */
public class SwitchMain {


   /* 后面的预览功能
   此代码受益于使用模式instanceof表达式，但远非完美。
   static String formatterPatternSwitch(Object o) {
        return switch (o) {
            case Integer i -> String.format("int %d", i);
            case Long l    -> String.format("long %d", l);
            case Double d  -> String.format("double %f", d);
            case String s  -> String.format("String %s", s);
            default        -> o.toString();
        };
    }*/
    static String formatter(Object o) {
       String formatted = "unknown";
       if (o instanceof Integer i) {
           formatted = String.format("int %d", i);
       } else if (o instanceof Long l) {
           formatted = String.format("long %d", l);
       } else if (o instanceof Double d) {
           formatted = String.format("double %f", d);
       } else if (o instanceof String s) {
           formatted = String.format("String %s", s);
       }
       return formatted;
    }

/*
    switch (day) {
        case MONDAY:
        case FRIDAY:
        case SUNDAY:
            System.out.println(6);
            break;
        case TUESDAY:
            System.out.println(7);
            break;
        case THURSDAY:
        case SATURDAY:
            System.out.println(8);
            break;
        case WEDNESDAY:
            System.out.println(9);
            break;

        //jdk 14 进阶写法
       switch (day) {
            case MONDAY, FRIDAY, SUNDAY -> System.out.println(6);
            case TUESDAY                -> System.out.println(7);
            case THURSDAY, SATURDAY     -> System.out.println(8);
            case WEDNESDAY              -> System.out.println(9);
}

    }*/


    static void testFooBar(String s) {
        if (s == null) {
            System.out.println("oops!");
            return;
        }
        switch (s) {
            case "Foo", "Bar" -> System.out.println("Great");
            default           -> System.out.println("Ok");
        }
    }

   /*
    预览功能尝试将null作为case判断项
    static void testFooBar(String s) {
        switch (s) {
            case null         -> System.out.println("Oops");
            case "Foo", "Bar" -> System.out.println("Great");
            default           -> System.out.println("Ok");
        }
    }*/

    /*class Shape {}
    class Rectangle extends Shape {}
    class Triangle  extends Shape { int calculateArea() { ... } }

    static void testTriangle(Shape s) {
        switch (s) {
            case null:
                break;
            case Triangle t:
                if (t.calculateArea() > 100) {
                    System.out.println("Large triangle");
                    break;
                }
            default:
                System.out.println("A shape, possibly a small triangle");
        }
    }*/


   /*
    密封类结合switch
    sealed interface S permits A, B, C {}
    final class A implements S {}
    final class B implements S {}
    record C(int i) implements S {}  // Implicitly final

    static int testSealedCoverage(S s) {
        return switch (s) {
            case A a -> 1;
            case B b -> 2;
            case C c -> 3;
        };
    }*/
}
