package dxt.learn.example.jdk17.streamFilter;

import java.io.*;

/**
 * JEP415  反序列化过滤器
 */
public class JEP415 {


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Dog dog = new Dog("哈士奇");
        dog.setPoc(new Poc());
        // 序列化 - 对象转字节数组
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try  {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(dog);
        }catch (Exception e){
        }
        byte[] bytes = byteArrayOutputStream.toByteArray();
        // 反序列化 - 字节数组转对象
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        // 允许 dxt.learn.example.jdk17.streamFilter.Dog 类，允许 java.base 中的所有类，拒绝其他任何类
        ObjectInputFilter filter = ObjectInputFilter.Config.createFilter(
                "dxt.learn.example.jdk17.streamFilter.Dog;java.base/*;!*");
        //dxt.learn.example.jdk17.streamFilter.*
        objectInputStream.setObjectInputFilter(filter);
        Object object = objectInputStream.readObject();
        System.out.println(object.toString());
    }
}
