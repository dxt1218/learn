package dxt.learn.example.jdk17.streamFilter;

import lombok.Data;

import java.io.Serializable;

/**
 */
@Data
public class Dog implements Serializable {

    private String type;

    private Poc poc;

    public Dog() {
    }
    public Dog(String type) {
        this.type = type;
    }
}
