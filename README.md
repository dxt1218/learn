# 从JDK8飞升到JDK17，再到未来的JDK21
2022年Spring6和 SpringBoot3相继推出，在此之前，Java社区一直是"新版任你发，我用Java 8"，不管新版本怎么出，很少有人愿意升级。
这一次，Spring 直接来了个大招，SpringBoot3和Spring6的最低依赖就是JDK17！跨过 JDK 8-16，直接升级到 JDK 17。那么为什么是 JDK 17呢？
## 为什么是JDK17
这么多新版本的 JDK，而且2022年还会推出 JDK 18 和 JDK 19，为什么 Spring 选择了 JDK 17呢。
主要是因为他是一个 Oracle官宣可以免费商用的LTS版本，所谓 LTS，是 Long Term Support，也就是官方保证会长期支持的版本。
![](img.png)
上面这张图是 Oracle 官方给出的 Oracle JDK 支持的时间线。可以看得到，JDK 17 最多可以支持到 2029 年 9 月份。按照技术更新迭代的速度，这次免费商用 8 年可谓是良苦用心，为的就是让使用者放心大胆地将 JDK 升级到 JDK 17(不过JDK 8 支持的时间更长，可以延长到 2030 年 12 月,JDK8可谓是YYDS！)
从 JDK 诞生到现在，还在长期支持的版本主要有 JDK 7、JDK 8 、JDK 11以及 JDK 1，JDK 17 将是继 Java 8 以来最重要的LTS版本，是 Java 社区八年努力的成果。

## 从JDK8到JDK17的新特性

### JDK9新特性（2017年9月）

* 模块化
* 提供了List.of()、Set.of()、Map.of()和Map.ofEntries()等工厂方法
* 接口支持私有方法
* Optional 类改进
* 多版本兼容Jar包
* JShell工具
* try-with-resources的改进
* Stream API的改进
* 设置G1为JVM默认垃圾收集器
* 支持http2.0和websocket的API

**重要特性：主要是API的优化，如支持HTTP2的Client API、JVM采用G1为默认垃圾收集器。**


### JDK10新特性（2018年3月）

局部变量类型推断，类似JS可以通过var来修饰局部变量，编译之后会推断出值的真实类型

不可变集合的改进

并行全垃圾回收器 G1，来优化G1的延迟

线程本地握手，允许在不执行全局VM安全点的情况下执行线程回调，可以停止单个线程，而不需要停止所有线程或不停止线程

Optional新增orElseThrow()方法

类数据共享

Unicode 语言标签扩展

根证书
重要特性：通过var关键字实现局部变量类型推断，使Java语言变成弱类型语言、JVM的G1垃圾回收由单线程改成多线程并行处理，降低G1的停顿时间。






















