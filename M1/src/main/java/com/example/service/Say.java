package com.example.service;

/**
 * @author DuanXT
 * @date 2022/12/12 15:31
 */
public interface Say {

    void talk() ;
}
