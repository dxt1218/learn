import com.example.service.Hi;
import com.example.service.Say;

/**
 * @author DuanXT
 * @date 2022/12/12 15:31
 */
module M1hi {
    exports com.example.service ;
    provides  Say with Hi;

    /**
     *
     *
     * 语法 exports 包名 [to] 模块名
     *     exports <package>;
     *     exports <package> to <module1>, <module2>...;
     *
     * opens关键字
     * open:该关键字如果加载模块上，那么通过exports的导出包下的类可见度是最高的，我们可以通过反射的方式来创建对对象和访问属性。
     * opens com.jdojo.claim.model;
     */



}

